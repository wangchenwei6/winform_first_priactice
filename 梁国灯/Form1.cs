﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //窗体加载完成之前执行这里的代码
            this.Text = "这是由窗体_Load事件修改的标题";
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            //Form2 form2 = new Form2();
            //form2.ShowDialog(); //对话框窗口（无法在子页背后访问父页）
            //MessageBox.Show("消息内容", "消息标题"); //MessageBox：消息框
            //MessageBox.Show("消息内容", "消息标题",MessageBoxButtons.YesNo); //是或否按钮\
            //MessageBox.Show("消息内容", "消息标题", MessageBoxButtons.YesNoCancel); //是、否及取消按钮
            //MessageBox.Show("错误内容", "错误标题", MessageBoxButtons.YesNo, MessageBoxIcon.Error); //消息图标：错误样式
            //MessageBox.Show("消息内容", "消息标题", MessageBoxButtons.YesNo, MessageBoxIcon.Information); //消息图标：消息样式
            
            DialogResult res = MessageBox.Show("消息内容","消息标题",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (res == DialogResult.Yes) //当res为Yes时
            {
                MessageBox.Show("选择了Yes");
            }
            else
            {
                MessageBox.Show("选择了No");
            }
            
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("登录成功！");
        }

    }
}
