﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Blue;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            MessageBox.Show("欢迎进入！");
        }

        private void Form2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            BackColor = Color.Black;
        }

        private void Form2_Click(object sender, EventArgs e)
        {
            var r = MessageBox.Show("点同意！", "请选择", MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (r == DialogResult.Yes)
            {
                MessageBox.Show("点同意，你就是我的人！");
            }
            else
            {
                MessageBox.Show("点不同意，我就是你的人！");
            }
         
        }
    }
}
